module Main exposing (main)

import Html exposing (..)
import Json.Decode exposing (..)
import Json.Decode.Pipeline exposing (..)
import MedicalRecords exposing (MedicalRecord)


main =
    Html.programWithFlags
        { init = init
        , update = \_ model -> ( model, Cmd.none )
        , subscriptions = \_ -> Sub.none
        , view = view
        }


type alias Model =
    { records : Result String (List MedicalRecord)
    }


init : Value -> ( Model, Cmd msg )
init value =
    let
        records =
            decodeValue recordsDecoder value
                |> Result.map (List.filterMap identity)
                |> Result.map MedicalRecords.filter
    in
        ( { records = records }
        , Cmd.none
        )


recordsDecoder : Decoder (List (Maybe MedicalRecord))
recordsDecoder =
    decode MedicalRecord
        |> required "id" int
        |> optional "name" string "N/A"
        |> required "screening_score" int
        |> required "started_at" int
        |> required "completed_at" int
        |> maybe
        |> list


view model =
    case model.records of
        Err string ->
            text string

        Ok records ->
            table [] <|
                [ tr []
                    [ th [] [ text "Name" ]
                    , th [] [ text "ID" ]
                    , th [] [ text "Score" ]
                    ]
                ]
                ++ (List.map recordView records)


recordView record =
    tr []
        [ th [] [ text record.name ]
        , th [] [ text (toString record.id) ]
        , th [] [ text (toString record.screeningScore) ]
        ]
