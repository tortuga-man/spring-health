module MedicalRecords exposing (..)


type alias MedicalRecord =
    { id : Int
    , name : String
    , screeningScore : Int
    , startedAt : Int
    , completedAt : Int
    }


filter : List MedicalRecord -> List MedicalRecord
filter =
    List.filter (\x -> x.screeningScore > 9 && x.completedAt - x.startedAt > 30)
