module Tests exposing (..)

import Test exposing (..)
import Expect
import Fuzz exposing (list, int, tuple, string)
import String
import MedicalRecords


lowScorePatient =
    { id = 1
    , name = "Pooh"
    , screeningScore = 8
    , startedAt = 2
    , completedAt = 33
    }


highScorePatient =
    { id = 2
    , name = "Eeyore"
    , screeningScore = 10
    , startedAt = 2
    , completedAt = 33
    }


fastPatient =
    { id = 3
    , name = "Tigger"
    , screeningScore = 10
    , startedAt = 2
    , completedAt = 4
    }


all : Test
all =
    describe "filtering medical records"
        [ test "it selects only patients who are depressed"
            <| \() ->
                Expect.equal (MedicalRecords.filter [lowScorePatient, highScorePatient]) [highScorePatient]
        , test "it does not select patients who are depressed but took fewer than 30 seconds to complete"
            <| \() ->
                Expect.equal (MedicalRecords.filter [highScorePatient, fastPatient]) [highScorePatient]
        ]
