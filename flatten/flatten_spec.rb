require_relative './flatten'

RSpec.describe "#flatten" do
  it "flattens an array of arbitrarily nested arrays of integers into a flat array of integers" do
    expect(
      flatten [[1, 2, [3]], 4]
    ).to eq(
      [1, 2, 3, 4]
    )

    expect(
      flatten [1, [2, 3], [4, 5, [6, 7]]]
    ).to eq(
      [1, 2, 3, 4, 5, 6, 7]
    )
  end

  it "does not alter a one-dimensional array" do
    expect(
      flatten [6, 5, 4, 3, 2]
    ).to eq(
      [6, 5, 4, 3, 2]
    )
  end

  it "raises an argument error when it is passed anything but an array" do
    expect { flatten(nil) }.to raise_error(ArgumentError)
    expect { flatten(1) }.to raise_error(ArgumentError)
    expect { flatten("foo") }.to raise_error(ArgumentError)
    expect { flatten({ "foo" => "bar" }) }.to raise_error(ArgumentError)
  end

  it "raises an argument error when the multi-dimensional array contains non-integers" do
    expect { flatten([nil]) }.to raise_error(ArgumentError)
    expect { flatten([1, [2, "foo"]]) }.to raise_error(ArgumentError)
  end
end
