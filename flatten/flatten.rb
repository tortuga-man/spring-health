def flatten(array)
  raise ArgumentError unless array.is_a? Array

  array.reduce([]) do |flat_array, element|
    if element.is_a? Array
      flat_array + flatten(element)
    elsif element.is_a? Integer
      flat_array + [ element ]
    else
      raise ArgumentError
    end
  end
end
