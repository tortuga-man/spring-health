# Spring Health Tech Screen

## Proudest Technical Achievement

I'm very proud of the [Cofounder Equity Split](https://cofounders.gust.com) that a team of mine built. I was on the project as both tech lead and PM, and we built a stellar product in under a month. I view this more than anything else as a testament to my ability to lead small teams composed of smart people whose expertises span multiple disiplines.

If I can cheat a little and mention two things, I'm also recently proud of myself for delivering a conference talk at Elm Europe and sharing a stage with some of my programming idols. If you're interested, you can watch that [here](https://www.youtube.com/watch?v=C3mnyJlCqMk&t=1s).

## Technical Book/Article

It is not difficult to find technical manuals in which the information presented is clear, concise and well-organized. Technical writing is an important skill, but our profession has no shortage of competent writers.

It is much more difficult to find authors who can capture key insights about technical leadership. Yonatan Zunger, a former Googler, recently responded to the so-called "Googler manifesto" aptly on Medium. Read it [here](https://medium.com/@yonatanzunger/so-about-this-googlers-manifesto-1e3773ed1788).

> Essentially, engineering is all about cooperation, collaboration, and empathy for both your colleagues and your customers.

This is something I believe deeply but with which a lot of engineers seem to struggle. It's an excellent read for anyone who considers themselves a technical leader.

## What I like about Spring

You're solving a real problem. That is, you identified a problem that exists *demonstrably* and are trying to fix it.

As someone who currently works for a startup which starts startups, I know that most new high-growth companies are not solving real problems. They are usually manufacturing scarcity, selling a brand or experience, or struggling to find traction in a limited market.

The fact that you've started with a problem, not with a brand or a technology (like every blockchain company out there), means that you're already miles ahead.

## Array Flattener

1. `cd flatten`
2. I used Ruby v2.4.1 to complete this challenge. If you encounter trouble, make sure you're using this version.
3. Make sure `rspec` is installed for your Ruby version. If not: `gem install rspec`
4. Run the test file: `rspec flatten_spec.rb`.
5. Look at those sweet green dots!

## Patient Records

I figured I would do one of these challenges in Elm, just to show you what it's all about. I realized too late that the problem was not necessarily well-suited to Elm, because it required marshaling JSON onto a web page. Nonetheless I forged ahead and made something quite a bit more complex than its Ruby counterpart would have been. Hopefully this will be more interesting to you!

1. `cd test_records`
2. Install elm and its testing library. Assuming you have npm installed: `npm install -g elm elm-test`
3. Run the tests: `elm test`
4. Build the JavaScript file (Elm compiles to JavaScript): `./build`
5. Open the resulting file `index.html` with *Firefox* (Chrome has rigid CORS protection and will prevent the local JSON file from being loaded, d'oh!)
